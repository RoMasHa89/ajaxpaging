(function($) {

	function find_page_number( element ) {
		element.find('span').remove();
		return parseInt( element.html() );
	}

	$(document).on( 'click', '.nav-links a', function( event ) {
		event.preventDefault();

		var page = find_page_number( $(this).clone() );

		$.ajax({
			url: ajaxpagination.ajaxurl,
			type: 'post',
			data: {
				action: 'ajax_pagination',
				page: page
			},
			success: function( html ) {
				$('#main').find( 'article' ).remove();
				$('#main nav').remove();
				$('#main').append( html );
				$('html, body').animate({ scrollTop: 0 }, 0);
			}
		})
	})
})(jQuery);