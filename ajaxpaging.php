<?php
/*
Plugin Name: Ajax Page Selector
Plugin URI: --
Description: Plugin, that provides page selection without reload
Version: 1.0.0
Author: Alex Romantsov
Author URI: --
License: No Licence
*/

function ajax_paging_enqueuer() {
    global $wp_query;

    // Check for empty query and begin ajax paging
    if ($wp_query->query == NULL ) {
        // code to embed th  java script file that makes the Ajax request
        wp_enqueue_script( 'ajax-pagination', plugins_url( 'js/ajax-pagination.js', __FILE__ ), array('jquery'), '1.0', true );
        // code to declare the URL to the file handling the AJAX request </p>
        wp_localize_script( 'ajax-pagination', 'ajaxpagination', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        ) );
    }
}
add_action( 'wp_enqueue_scripts', 'ajax_paging_enqueuer' );


add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );

function my_ajax_pagination() {

    $query_vars['paged'] = $_POST['page'];

    $posts = new WP_Query( $query_vars );
    $GLOBALS['wp_query'] = $posts;

    if( ! $posts->have_posts() ) {
        get_template_part( 'content', 'none' );
    }
    else {
        while ( $posts->have_posts() ) {
            $posts->the_post();
            get_template_part( 'content', get_post_format() );
        }
    }

    the_posts_pagination( array(
        'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
        'next_text'          => __( 'Next page', 'twentyfifteen' ),
        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
    ) );

    die();
}
